import GetOldTweets3 as got
import numpy as np

def save_to_file(FILENAME):
    return 1

def grab_tweets(USERNAME,NUM):

    tweetCriteria = got.manager.TweetCriteria().setUsername(USERNAME)\
                                           .setMaxTweets(NUM)
    tweets=""
    for T in got.manager.TweetManager.getTweets(tweetCriteria):
        #print (T.text)
        tweets+=T.text
        tweets+=" "
    return tweets

def make_pairs(corpus):
    for i in range(len(corpus)-1):
        yield (corpus[i], corpus[i+1])

def generate_from_single_source(corpus):
    pairs = make_pairs(corpus)

    word_dict = {}

    for word_1, word_2 in pairs:
        if word_1 in word_dict.keys():
            word_dict[word_1].append(word_2)
        else:
            word_dict[word_1] = [word_2]
     
    first_word = np.random.choice(corpus)

    while first_word.islower():
        first_word = np.random.choice(corpus)
        #print(first_word)

    chain = [first_word]

    n_words = 50

    for i in range(n_words):
        chain.append(np.random.choice(word_dict[chain[-1]]))

    OUT=' '.join(chain)
    print(OUT)
    
def main():

    OBAMA = grab_tweets("BarackObama",10000)
    TRUMP= grab_tweets("POTUS",10000)
    corpus=OBAMA+TRUMP
    corpus=corpus.split()

    for X in range(100):
        print(X)
        generate_from_single_source(corpus)
main()







